package ru.nsu.fit.grigor.networksecond.client

import java.io.File
import java.math.BigInteger
import java.net.Socket
import java.nio.charset.Charset
import kotlin.system.exitProcess


class Client(filePath: String, host: String, port: Int) {
    private val file: File = File(filePath)
    private val socket: Socket = Socket(host, port)
    private val inputStream = file.inputStream()

    enum class Result(val result: Int) {
        RESULT_OK(1),
        RESULT_FAIL(0)
    }

    private fun sendBytes(bytes: ByteArray) {
        socket.getOutputStream().write(bytes)
    }

    private fun sendBytes(bytes: ByteArray, length: Int) {
        when {
            length == bytes.size -> {
                socket.getOutputStream().write(bytes)
            }
            length > bytes.size -> {
                val extendedBytes = ByteArray(length)
                System.arraycopy(bytes, 0, extendedBytes, length - bytes.size, bytes.size)
                socket.getOutputStream().write(extendedBytes)
            }
            else -> {
                val newBytes: ByteArray = bytes.copyOf(length)
                socket.getOutputStream().write(newBytes)
            }
        }

    }

    fun sendFile() {
        println("Transfer begins.........")
        val fileName = file.name.toByteArray(Charset.forName("UTF-8"))
        println("File name size.....")
        sendBytes(BigInteger.valueOf(fileName.size.toLong()).toByteArray(), 2)

        println("File name......")
        sendBytes(fileName)

        println("File size......")
        sendBytes(BigInteger.valueOf(file.length()).toByteArray(), 8)

        try {
            startSending()
        } catch (e: Exception) {
            println("Something went wrong during upload. Error: " + e.localizedMessage)
            exitProcess(0)
        }

        println("File has been send successfully!")//todo: not correct
        inputStream.close()
    }

    private fun startSending() {
        val buffer = ByteArray(4096)
        println("Sending file...")
        var readBytes: Int
        do {
            readBytes = inputStream.read(buffer)
            if (readBytes == -1) {
                return
            }
            sendBytes(buffer, readBytes)
            Thread.sleep(10)
        } while (readBytes > 0)
        try {
            val result = socket.getInputStream().read().toByte()
            if (result.toInt() == Result.RESULT_OK.result) {
                println("File uploaded successfully!")
            }
        } catch (e: Exception) {

        }
    }
}