package ru.nsu.fit.grigor.networksecond.client

fun main(args: Array<String>) {
    if (args.size < 3) {// file_path; server IP-address; server port
        println("Too few arguments. Should be 3.")
        return
    }

    val client = Client(args[0], args[1], Integer.parseInt(args[2]))
    client.sendFile()
}