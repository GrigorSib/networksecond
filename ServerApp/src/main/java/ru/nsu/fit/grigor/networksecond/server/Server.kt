package ru.nsu.fit.grigor.networksecond.server

import java.io.File
import java.net.ServerSocket
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class Server(port: Int) {
    private var pool1: ExecutorService = Executors.newFixedThreadPool(6)
    private val pool: ExecutorService = Executors.newCachedThreadPool()
    private val serverSocket: ServerSocket = ServerSocket(port, 100)
    private var clientCount = 0
        get() {
            if (field % 100 == 0) {
                field = 0
            }
            return field
        }

    var serverOnline: Boolean = false

    init {
        File("uploads").mkdir()
    }

    fun startListening() {
        serverOnline = true

        while (serverOnline) {
            val socket = serverSocket.accept()
            clientCount++
            if (clientCount % 100 == 0) {
                clientCount = 0
            }
            val clientName = "Client-$clientCount"
            pool.submit(ReadTask(socket, clientName))
        }
    }
}