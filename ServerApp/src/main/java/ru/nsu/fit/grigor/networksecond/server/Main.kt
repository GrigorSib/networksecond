package ru.nsu.fit.grigor.networksecond.server

fun main(args: Array<String>) {
    if (args.isEmpty()) {
        println("Port number must be specified.")
    }
    Server(args[0].toInt()).startListening()

}