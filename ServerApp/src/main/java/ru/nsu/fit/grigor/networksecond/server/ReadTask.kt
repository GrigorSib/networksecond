package ru.nsu.fit.grigor.networksecond.server

import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.math.BigInteger
import java.net.Socket
import java.util.*


class ReadTask(private val socket: Socket, private val client: String) : Runnable {
    private val inputStream: InputStream = socket.getInputStream()
    private var fileOutputStream: FileOutputStream? = null
    private val timer = Timer()

    override fun run() {
        try {
            val filenameLength = filenameLength()
            val filename = getFilename(filenameLength)
            val fileSize = fileSize()
            val file = createAndGetFile(filename)
            fileOutputStream = file.outputStream()
            readFile(fileSize, fileOutputStream!!)
        } catch (e: Exception) {
            print(getErrorMessage(e))
            sendResult(Result.RESULT_FAIL)
            freeResources()
            return
        }

        sendResult(Result.RESULT_OK)
        println("${client}.\nFile has been successfully downloaded!!!!")
        freeResources()
    }

    private fun sendResult(result: Result) {
        socket.getOutputStream().write(result.result)
        socket.getOutputStream().flush()
    }

    private fun getErrorMessage(e: Exception): String {
        val builder = StringBuilder()
        builder.append("${client}\nSomething went wrong " +
                "while downloading file. Error: ${e.localizedMessage}\n")
                .append("\nConnection's closed.")
        return builder.toString()
    }

    private fun freeResources() {
        timer.cancel()
        socket.close()
        fileOutputStream?.close()
    }

    private fun readFile(fileSize: ByteArray, outputStream: FileOutputStream) {
        val buffer = ByteArray(4096)
        val bytesReadForTimer = mutableListOf(1)
        var bytesRead: Int
        var bytesLeft = BigInteger(fileSize).toLong() // todo: is this Ok? What if file size is too big?
        timer.schedule(SpeedCounterTask(bytesReadForTimer, client), 0, 3000)

        do {
            bytesRead = inputStream.read(buffer, 0,
                    if (bytesLeft < 4096L)
                        Math.toIntExact(bytesLeft)
                    else 4096
            )
            bytesLeft -= bytesRead
            bytesReadForTimer[0] += bytesRead
            outputStream.write(buffer, 0, bytesRead)
        } while (bytesLeft > 0)
    }

    private fun createAndGetFile(filename: ByteArray): File {
        val file = File("uploads/" + String(filename))
        return file
    }

    private fun fileSize(): ByteArray {
        val fileLength = ByteArray(8)// todo: wtf?
        inputStream.read(fileLength, 0, fileLength.size)
        return fileLength
    }

    private fun getFilename(filenameSize: ByteArray): ByteArray {
        val filename = ByteArray(BigInteger(filenameSize).intValueExact())
        inputStream.read(filename, 0, filename.size)
        return filename
    }

    private fun filenameLength(): ByteArray {
        val filenameSize = ByteArray(2) // less than 4096 symbols
        inputStream.read(filenameSize, 0, 2)
        return filenameSize
    }

}
