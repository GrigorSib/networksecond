package ru.nsu.fit.grigor.networksecond.server

enum class Result(val result: Int) {
    RESULT_OK(1),
    RESULT_FAIL(0)
}