package ru.nsu.fit.grigor.networksecond.server

import java.lang.StringBuilder
import java.util.*

class SpeedCounterTask(private val bytesRead: MutableList<Int>, val client: String) : TimerTask() {
    private var tickAmount = 0
    private var overallBytesRead = 0

    override fun run() {
        tickAmount++
        overallBytesRead += bytesRead[0]
        val builder = StringBuilder()
        builder.append(client + "\n")
                .append(String.format("Average speed: %.3f MB/s\n", overallBytesRead / tickAmount / 1024.0 / 1024.0 / 3))
                .append(String.format("Current speed: %.3f MB/s\n", bytesRead[0] / 1024.0 / 1024.0 / 3))
        print(builder.toString())
        bytesRead[0] = 0
    }

}